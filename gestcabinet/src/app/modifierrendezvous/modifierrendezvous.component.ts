import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RendezvousserviceService } from '../services/rendezvousservice.service';

@Component({
  selector: 'app-modifierrendezvous',
  templateUrl: './modifierrendezvous.component.html',
  styleUrls: ['./modifierrendezvous.component.css']
})
export class ModifierrendezvousComponent implements OnInit {

  public rendezvouscourant;
  public url:string;
  constructor(private router: Router, private routeactive : ActivatedRoute ,
              private rendezvouservice : RendezvousserviceService) { }

  ngOnInit(): void {
    this.url = atob(this.routeactive.snapshot.params.id);
    this.rendezvouservice.getRendezVousByUrl(this.url)
    .subscribe(data =>{
      this.rendezvouscourant=data;
     }, error=>{
       console.log(error);
       
     });
  }

  modifierRendezVous(donnee){
    this.rendezvouservice.modifierRendezVous(this.url, donnee)
    .subscribe(data =>{
      alert("Rendez-Vous Modifier avec succes");
      this.router.navigateByUrl("/rendezvous");
     }, error=>{
       console.log(error);
       
     });
  }

}
