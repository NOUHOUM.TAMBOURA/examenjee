import { Injectable } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';

declare let Keycloak: any;
@Injectable({
  providedIn: 'root'
})
export class KeycloakServiceService {
public kc:KeycloakInstance;
  constructor() { }

 public async initial(){

  this.kc= new Keycloak ({
    url: "http://localhost:8080/auth",
    realm:"Cabinet-Medical",
    clientId:"gescabinet"
  });

  await this.kc.init({
    onLoad: 'login-required'
  });


  }
}
