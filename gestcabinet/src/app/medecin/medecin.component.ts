import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MedecinserviceService } from '../services/medecinservice.service';

@Component({
  selector: 'app-medecin',
  templateUrl: './medecin.component.html',
  styleUrls: ['./medecin.component.css']
})
export class MedecinComponent implements OnInit {

  public listemedecin;
  public size:number=5;
  public pagecourant:number=0;
  public pagestotal:number;
  public pages:Array<number>;
  constructor(private medecinservice: MedecinserviceService , 
    private router:Router) { }

  ngOnInit(): void {

    this.medecinservice.getMedecin(this.pagecourant, this.size)
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listemedecin=data;
    }, error=>{
      console.log(error);
      
    });

  }

  pageMedecin(i){
    this.pagecourant=i;
    this.ngOnInit();
  }


  chercher(value: any){
    console.log(value);

    this.medecinservice.getMedecinByNom(value.nommedecin, this.pagecourant,this.size )
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listemedecin=data;
    }, error=>{
      console.log(error);
      
    });

  }

  onDelete(medecin){
    let conf = confirm("Etes Vous Sure !!!!");
    if(conf){
      console.log(medecin);
      this.medecinservice.deleteMedecin(medecin._links.self.href)
      .subscribe(data =>{
       this.ngOnInit();
      }, error=>{
        console.log(error);
        
      });
    }
  }

  onUpdate(medecin){
    let url=medecin._links.self.href;
    this.router.navigateByUrl("modifiermedecin/"+btoa(url));
  }

  ajoutermedecin(donnee : any){
    console.log(donnee);
    this.medecinservice.ajouter(this.medecinservice.url+"/medecins",donnee)
    .subscribe(data =>{
      console.log(data);
      this.ngOnInit();
     }, error=>{
       console.log(error);
       
     });

  }

}
