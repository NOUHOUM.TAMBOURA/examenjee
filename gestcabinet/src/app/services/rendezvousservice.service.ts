import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RendezvousserviceService {

  public url:string="http://localhost:2000/RENDEZ-VOUS-API";

  constructor(private httpClient: HttpClient) { }

  public getRendezVous(page:number,size:number){
    return this.httpClient.get(this.url+"/rendezVouses?page="+page+"&size="+size);
  }

  public getRendezVousByDate(date: Date, page:number,size:number){
    return this.httpClient.get(this.url+"/patients/search/cherchernom?date="+date+"&page="+page+"&size="+size);
  }

  // pour recuperer la liste des patient et medecin

  public getMedecin(){
    return this.httpClient.get("http://localhost:2000/MEDECIN-SERVICE/allMedecin");
  }

  public getPatient(){
    return this.httpClient.get("http://localhost:2000/PATIENT-SERVICE/allPatient");
  }
  public deleteRendezVous(url){
    return this.httpClient.delete(url);
  }

  public ajouter(url , donnee){
    return this.httpClient.post(url ,donnee);
  }

  public getDetailsRendezVous(rdv){
    return this.httpClient.get(this.url+"/detailsRendezVous/"+rdv);
  }

  getRendezVousByUrl(url){
    return this.httpClient.get(url);
  }
  modifierRendezVous(url , donnee){
    return this.httpClient.put(url , donnee);
  }

}
