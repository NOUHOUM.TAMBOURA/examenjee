import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConsultationserviceService {

  public url:string="http://localhost:2000/CONSULTATION-API";  

  constructor(private httpClient : HttpClient) { }

  public getConsultation(page:number,size:number){
    return this.httpClient.get(this.url+"/consultations?page="+page+"&size="+size);
  }

  public getPatient(){
    return this.httpClient.get("http://localhost:2000/PATIENT-SERVICE/allPatient");
  }

  public getRendezVous(){
    return this.httpClient.get("http://localhost:2000/RENDEZ-VOUS-API/AllRendezVous");
  }

  public ajouterConsultation(url , donnee){
    return this.httpClient.post(url ,donnee);
  }

  public getDetailsConsultation(cons){
    
    return this.httpClient.get(this.url+"/detailsconsultations/"+cons);
  }

  public getConsultationByDate(date: Data, page:number,size:number){
    return this.httpClient.get(this.url+"/consultations/search/cherchernom?dateconsultation="+date+"&page="+page+"&size="+size);
  }

  public deleteConsultation(url){
    return this.httpClient.delete(url);
  }

  public getConsultationByUrl(url){
    return this.httpClient.get(url);
  }
  modifierConsultation(url , donnee){
    return this.httpClient.put(url , donnee);
  }



}
