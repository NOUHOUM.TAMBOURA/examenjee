import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PatientserviceService {

  public url:string="http://localhost:2000/PATIENT-SERVICE";

  constructor(private httpClient: HttpClient) { }


  // methode pour recuperer la liste des medecin pour la classe consultation 
  public getDetailsPatientConsultation(id : number){
    return this.httpClient.get(this.url+"/patients/"+id);
  }

  public getPatient(page:number,size:number){
    return this.httpClient.get(this.url+"/patients?page="+page+"&size="+size);
  }

  public getPatientByNom(nom: String, page:number,size:number){
    return this.httpClient.get(this.url+"/patients/search/cherchernom?nom="+nom+"&page="+page+"&size="+size);
  }

  public ajouter(url , donnee){
    return this.httpClient.post(url ,donnee);
  }
  
  public deletePatient(url){
    return this.httpClient.delete(url);
  }

  getPatientByUrl(url){
    return this.httpClient.get(url);
  }

  public modifierpatient(url , donnee){
    return this.httpClient.put(url ,donnee);
  }
}
