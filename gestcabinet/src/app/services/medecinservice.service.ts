import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MedecinserviceService {

  public url:string="http://localhost:2000/MEDECIN-SERVICE";

  constructor(private httpClient: HttpClient) { }


  // methode pour recuperer la liste des medecin pour la classe consultation 

  public getDetailsMedecinConsultation(id: number){
    return this.httpClient.get(this.url+"/medecins/"+id);
  }

  public getMedecin(page:number,size:number){
    return this.httpClient.get(this.url+"/medecins?page="+page+"&size="+size);
  }

  public getMedecinByNom(nom: string, page:number,size:number){
    return this.httpClient.get(this.url+"/medecins/search/cherchernom?nom="+nom+"&page="+page+"&size="+size);
  }

  public ajouter(url , donnee){
    return this.httpClient.post(url ,donnee);
  }
    
  public deleteMedecin(url){
    return this.httpClient.delete(url);
  }

  public getMedecinByUrl(url) {
    return this.httpClient.get(url);
  }

  public modifiermedecin(url , donnee){
    return this.httpClient.put(url ,donnee);
  }

}
