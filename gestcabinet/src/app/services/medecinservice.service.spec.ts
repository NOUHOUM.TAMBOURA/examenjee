import { TestBed } from '@angular/core/testing';

import { MedecinserviceService } from './medecinservice.service';

describe('MedecinserviceService', () => {
  let service: MedecinserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MedecinserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
