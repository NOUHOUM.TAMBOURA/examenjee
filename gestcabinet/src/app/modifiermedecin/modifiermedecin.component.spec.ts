import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifiermedecinComponent } from './modifiermedecin.component';

describe('ModifiermedecinComponent', () => {
  let component: ModifiermedecinComponent;
  let fixture: ComponentFixture<ModifiermedecinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifiermedecinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifiermedecinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
