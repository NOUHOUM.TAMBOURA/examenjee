import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Medecin } from '../modelmedecin/modelmedecin';
import { MedecinserviceService } from '../services/medecinservice.service';

@Component({
  selector: 'app-modifiermedecin',
  templateUrl: './modifiermedecin.component.html',
  styleUrls: ['./modifiermedecin.component.css']
})
export class ModifiermedecinComponent implements OnInit {

  public medecincourant ;
  public url:string
  constructor(private router:Router, private routeactive: ActivatedRoute,
              private medecinservice: MedecinserviceService) { }

  ngOnInit(): void {
     this.url = atob(this.routeactive.snapshot.params.id);
    this.medecinservice.getMedecinByUrl(this.url)
    .subscribe(data =>{
      this.medecincourant=data;
     }, error=>{
       console.log(error);
       
     });
    console.log(this.url);
  }
  Onmodifiermedecin(donnee){
    this.medecinservice.modifiermedecin(this.url, donnee)
    .subscribe(data =>{
      alert("Medecin Modifier avec succes");
      this.router.navigateByUrl("/medecin");
     }, error=>{
       console.log(error);
       
     });
  }
}
