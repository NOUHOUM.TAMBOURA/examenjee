import { Component, OnInit } from '@angular/core';
import { KeycloakServiceService } from './servicesKeycloak/keycloak-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(public securitykeycloak : KeycloakServiceService){}

  title = 'gestcabinet';
  ngOnInit() : void {
  }

OnLogout(){
  console.log("logout");
  this.securitykeycloak.kc.logout();
}


}
