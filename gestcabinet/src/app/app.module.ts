import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { MedecinComponent } from './medecin/medecin.component';
import { PatientComponent } from './patient/patient.component';
import { ConsultationComponent } from './consultation/consultation.component';
import { RendezvousComponent } from './rendezvous/rendezvous.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ModifiermedecinComponent } from './modifiermedecin/modifiermedecin.component';
import { ModifierpatientComponent } from './modifierpatient/modifierpatient.component';
import { ModifierrendezvousComponent } from './modifierrendezvous/modifierrendezvous.component';
import { ModifierconsultationComponent } from './modifierconsultation/modifierconsultation.component';
import { KeycloakServiceService } from './servicesKeycloak/keycloak-service.service';





const appRouter : Routes= [
  { path: '', redirectTo:"/home", pathMatch:"full"},
  { path: 'home', component: HomeComponent},
  { path: 'medecin', component: MedecinComponent},
  { path: 'patient', component: PatientComponent},
  { path: 'rendezvous', component: RendezvousComponent},
  { path: 'consultation', component: ConsultationComponent},
  
  { path: 'modifiermedecin/:id', component: ModifiermedecinComponent},
  { path: 'modifierpatient/:id', component: ModifierpatientComponent},
  { path: 'modifierrendezvous/:id', component: ModifierrendezvousComponent},
  { path: 'modifieconsultation/:id', component: ModifierconsultationComponent}
  
  //{ path: '', redirectTo:"/login", pathMatch:"full"}
]

export function KeycloakFactory(keycloakService: KeycloakServiceService){
  return ()=>keycloakService.initial();
}

@NgModule({
  declarations: [
    AppComponent,
    MedecinComponent,
    PatientComponent,
    ConsultationComponent,
    RendezvousComponent,
    HomeComponent,
    ModifiermedecinComponent,
    ModifierpatientComponent,
    ModifierrendezvousComponent,
    ModifierconsultationComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRouter)
  ],
  providers: [
    {provide:APP_INITIALIZER,deps:[KeycloakServiceService],useFactory:KeycloakFactory,multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
