export class Medecin{
    public idmedecin: number;
    public nommedecin :string;
    public prenommedecin: string;
    public adressemedecin:string;
    public telmedecin: string;
    public dateNaissmedecin:Date;
    public sexemedecin:string;

}

