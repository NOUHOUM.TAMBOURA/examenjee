import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifierconsultationComponent } from './modifierconsultation.component';

describe('ModifierconsultationComponent', () => {
  let component: ModifierconsultationComponent;
  let fixture: ComponentFixture<ModifierconsultationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifierconsultationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifierconsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
