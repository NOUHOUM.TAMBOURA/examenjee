import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConsultationserviceService } from '../services/consultationservice.service';

@Component({
  selector: 'app-modifierconsultation',
  templateUrl: './modifierconsultation.component.html',
  styleUrls: ['./modifierconsultation.component.css']
})
export class ModifierconsultationComponent implements OnInit {

  public consultartioncourant;
  public url:string;
  constructor(private router: Router, private routeactive : ActivatedRoute ,
                   private consultationservice : ConsultationserviceService) { }

  ngOnInit(): void {
    this.url = atob(this.routeactive.snapshot.params.id);
    console.log(this.url);
    this.consultationservice.getConsultationByUrl(this.url)
    .subscribe(data =>{
      this.consultartioncourant=data;
     }, error=>{
       console.log(error);
       
     });
  }

  modifierConsultation(donnee){
    this.consultationservice.modifierConsultation(this.url, donnee)
    .subscribe(data =>{
      alert("Consultation Modifier avec succes");
      this.router.navigateByUrl("/consultation");
     }, error=>{
       console.log(error);
       
     });
  }

}
