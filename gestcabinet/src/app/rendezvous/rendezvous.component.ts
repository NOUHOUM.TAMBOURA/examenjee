import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RendezvousserviceService } from '../services/rendezvousservice.service';

@Component({
  selector: 'app-rendezvous',
  templateUrl: './rendezvous.component.html',
  styleUrls: ['./rendezvous.component.css']
})
export class RendezvousComponent implements OnInit {

  details;
  listemedecin;
  listepatient;
  listerendezvous;
  public size:number=5;
  public pagecourant:number=0;
  public pagestotal:number;
  public pages:Array<number>;

  public vue:number=1;
  constructor( private rendezvousservice: RendezvousserviceService,
            private  router: Router) { }

  ngOnInit(): void {
    this.rendezvousservice.getRendezVous(this.pagecourant, this.size)
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listerendezvous=data;
    }, error=>{
      console.log(error);
      
    });

    // pour la cle etrangere au niveau du rendezvous
    this.rendezvousservice.getMedecin()
    .subscribe(data =>{
      this.listemedecin=data;
    }, error=>{
      console.log(error);
      
    });

     // pour la cle etrangere au niveau du rendezvous
    this.rendezvousservice.getPatient()
    .subscribe(data =>{
      this.listepatient=data;
    }, error=>{
      console.log(error);
      
    });
  }

  Onchercher(value){
    console.log(value);

    this.rendezvousservice.getRendezVousByDate(value.daterendezvous, this.pagecourant,this.size )
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listerendezvous=data;
    }, error=>{
      console.log(error);
      
    });

  }

    // la methode pour la pagination
    pageRendezVous(i){
      this.pagecourant=i;
      this.ngOnInit();
    }
  
   
    ajouterrendezvous(donnee : any){
      console.log(donnee);
      this.rendezvousservice.ajouter(this.rendezvousservice.url+"/rendezVouses",donnee)
      .subscribe(data =>{
        console.log(data);
        this.ngOnInit();
       }, error=>{
         console.log(error);
         
       });
  
    }
  
    onDelete(rdv){
      let conf = confirm("Etes Vous Sure !!!!");
      if(conf){
        console.log(rdv);
        this.rendezvousservice.deleteRendezVous(rdv._links.self.href)
        .subscribe(data =>{
         this.ngOnInit();
        }, error=>{
          console.log(error);
          
        });
      }
    }

    detailRendezVous(rdv){
        this.rendezvousservice.getDetailsRendezVous(rdv.idrendezvous)
        .subscribe(data =>{
          this.details=data;
        //this.router.navigateByUrl("/details");
          this.vue=2;
          console.log(data);
         
         }, error=>{
           console.log(error);
           
         });
    }
  
    PageRendezVous(){
      this.vue=1;
    }

    onUpdate(rdv){
      let url=rdv._links.self.href;
      this.router.navigateByUrl("modifierrendezvous/"+btoa(url));
    }

}
