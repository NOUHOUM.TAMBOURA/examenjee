import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientserviceService } from '../services/patientservice.service';

@Component({
  selector: 'app-modifierpatient',
  templateUrl: './modifierpatient.component.html',
  styleUrls: ['./modifierpatient.component.css']
})
export class ModifierpatientComponent implements OnInit {

  public patientcourant ;
  public url:string
  constructor(private router:Router, private routeactive: ActivatedRoute,
              private patientservice: PatientserviceService) { }

  ngOnInit(): void {
    this.url = atob(this.routeactive.snapshot.params.id);
    this.patientservice.getPatientByUrl(this.url)
    .subscribe(data =>{
      this.patientcourant=data;
     }, error=>{
       console.log(error);
       
     });
  }

  Onmodifierpatient(donnee){
    this.patientservice.modifierpatient(this.url, donnee)
    .subscribe(data =>{
      alert("Paient Modifier avec succes");
      this.router.navigateByUrl("/patient");
     }, error=>{
       console.log(error);
       
     });
  }

}
