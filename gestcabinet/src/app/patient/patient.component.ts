import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PatientserviceService } from '../services/patientservice.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  listepatient;
  public size:number=5;
  public pagecourant:number=0;
  public pagestotal:number;
  public pages:Array<number>;
  constructor( private patientservice: PatientserviceService,
             private router : Router) { }

  ngOnInit(): void {
    this.patientservice.getPatient(this.pagecourant, this.size)
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listepatient=data;
    }, error=>{
      console.log(error);
      
    });
  }


  Onchercher(value){
    console.log(value);

    this.patientservice.getPatientByNom(value.nompatient, this.pagecourant,this.size )
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listepatient=data;
    }, error=>{
      console.log(error);
      
    });

  }
  // la methode pour la pagination
  pagePatient(i){
    this.pagecourant=i;
    this.ngOnInit();
  }

 
  

  onDelete(patient){
    let conf = confirm("Etes Vous Sure !!!!");
    if(conf){
      console.log(patient);
      this.patientservice.deletePatient(patient._links.self.href)
      .subscribe(data =>{
       this.ngOnInit();
      }, error=>{
        console.log(error);
        
      });
    }
  }

  onUpdate(patient){
    let url=patient._links.self.href;
    this.router.navigateByUrl("modifierpatient/"+btoa(url));
  }
  
  ajouterpatient(donnee : any){
    console.log(donnee);
    this.patientservice.ajouter(this.patientservice.url+"/patients",donnee)
    .subscribe(data =>{
      console.log(data);
      this.ngOnInit();
     }, error=>{
       console.log(error);
       
     });

  }


}
