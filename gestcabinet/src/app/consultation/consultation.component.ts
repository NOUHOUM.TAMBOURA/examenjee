import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultationserviceService } from '../services/consultationservice.service';
import { MedecinserviceService } from '../services/medecinservice.service';
import { PatientserviceService } from '../services/patientservice.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.css']
})
export class ConsultationComponent implements OnInit {

  public patientdetailconsultation;
  public medecindetailconsultation;
  public listeconsultation; 
  public listepatient;
  public listerendezvous;
  public details;

  public size:number=5;
  public pagecourant:number=0;
  public pagestotal:number;
  public pages:Array<number>;

  public vue:number=1;
  constructor( private consultationservice : ConsultationserviceService, 
              private router : Router , private medecinservice: MedecinserviceService ,
              private patientservice : PatientserviceService) { }

  ngOnInit(): void {

    this.consultationservice.getConsultation(this.pagecourant, this.size)
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listeconsultation=data;
    }, error=>{
      console.log(error);
      
    });


    // pour la cle etrangere au niveau du rendezvous
    this.consultationservice.getPatient()
    .subscribe(data =>{
      this.listepatient=data;
    }, error=>{
      console.log(error);
      
    });

     // pour la cle etrangere au niveau du rendezvous
    this.consultationservice.getRendezVous()
    .subscribe(data =>{
      this.listerendezvous=data;
    }, error=>{
      console.log(error);
      
    });
  }

  Onchercher(value){
    console.log(value);

    this.consultationservice.getConsultationByDate(value.dateconsultation, this.pagecourant,this.size )
    .subscribe(data =>{
      this.pagestotal= data["page"].totalPages;
      this.pages= new Array<number>(this.pagestotal);
      this.listeconsultation=data;
    }, error=>{
      console.log(error);
      
    });
  }

   // la methode pour la pagination
   pageRendezVous(i){
    this.pagecourant=i;
    this.ngOnInit();
  }

  ajouterconsultation(donnee : any){
    console.log(donnee);
    this.consultationservice.ajouterConsultation(this.consultationservice.url+"/consultations",donnee)
    .subscribe(data =>{
      console.log(data);
      this.ngOnInit();
     }, error=>{
       console.log(error);
       
     });

  }

  onDelete(consultation){
    let conf = confirm("Etes Vous Sure !!!!");
    if(conf){
      console.log(consultation);
      this.consultationservice.deleteConsultation(consultation._links.self.href)
      .subscribe(data =>{
       this.ngOnInit();
      }, error=>{
        console.log(error);
        
      });
    }
  }

  detailConsultation(consultation){
    this.consultationservice.getDetailsConsultation(consultation.idconsultation)
    .subscribe(data =>{
      console.log(consultation);
      this.details=data;
    //this.router.navigateByUrl("/details");
      this.vue=2;
      console.log(data);
     
     }, error=>{
       console.log(error);
       
     });
}

Pagecourant(){
  this.vue=1;
}

PageConsultation(i){
  this.pagecourant=i;
  this.ngOnInit();
}

// les methodes pour afficher les details quand on click sur une id 
afficherMedecin(details){
  let id=details.rendezVous.idmedecin;
  console.log(id);
  this.medecinservice.getDetailsMedecinConsultation(details.rendezVous.idmedecin)
  .subscribe(data =>{
    this.medecindetailconsultation=data;
  //this.router.navigateByUrl("/details");
    this.vue=3;
    console.log(data);
   
   }, error=>{
     console.log(error);
     
   });
}
// les methodes pour afficher les details quand on click sur une id 
afficherPatient(details){
  let id=details.rendezVous.idpatient;
  this.patientservice.getDetailsPatientConsultation(id)
  .subscribe(data =>{
    this.patientdetailconsultation=data;
    console.log(this.patientdetailconsultation);
  //this.router.navigateByUrl("/details");
    this.vue=4;
    console.log(data);
   
   }, error=>{
     console.log(error);
     
   });
}

onUpdate(consultation){
  let url=consultation._links.self.href;
  console.log(url);
  this.router.navigateByUrl("modifieconsultation/"+btoa(url));
}


}
