package com.isi.medecinservice.ControllerMedecin;

import com.isi.medecinservice.dao.MedecinRepository;
import com.isi.medecinservice.entite.Medecin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin("*")
@RestController
public class MedecinController {

    @Autowired
    private MedecinRepository medecinRepository;

        @GetMapping(value = "/allMedecin")
    public List<Medecin> getAllPatient(){
        return medecinRepository.findAll();
    }
}
