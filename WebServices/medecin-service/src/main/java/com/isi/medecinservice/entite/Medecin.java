package com.isi.medecinservice.entite;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Medecin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idmedecin;
    @Column(length = 50)
    private String nommedecin;
    @Column(length = 50)
    private String prenommedecin;
    @Column(length = 50)
    private String adressemedecin;
    @Column(length = 50)
    private String telmedecin;
    @Column(length = 50)
    private Date dateNaissmedecin;
    @Column(length = 50)
    private String sexemedecin;

    public Medecin() {
    }

    public Medecin(String nommedecin, String prenommedecin, String adressemedecin, String telmedecin, Date dateNaissmedecin, String sexemedecin) {
        this.nommedecin = nommedecin;
        this.prenommedecin = prenommedecin;
        this.adressemedecin = adressemedecin;
        this.telmedecin = telmedecin;
        this.dateNaissmedecin = dateNaissmedecin;
        this.sexemedecin = sexemedecin;
    }

    public long getIdmedecin() {
        return idmedecin;
    }

    public void setIdmedecin(long idmedecin) {
        this.idmedecin = idmedecin;
    }

    public String getNommedecin() {
        return nommedecin;
    }

    public void setNommedecin(String nommedecin) {
        this.nommedecin = nommedecin;
    }

    public String getPrenommedecin() {
        return prenommedecin;
    }

    public void setPrenommedecin(String prenommedecin) {
        this.prenommedecin = prenommedecin;
    }

    public String getAdressemedecin() {
        return adressemedecin;
    }

    public void setAdressemedecin(String adressemedecin) {
        this.adressemedecin = adressemedecin;
    }

    public String getTelmedecin() {
        return telmedecin;
    }

    public void setTelmedecin(String telmedecin) {
        this.telmedecin = telmedecin;
    }

    public Date getDateNaissmedecin() {
        return dateNaissmedecin;
    }

    public void setDateNaissmedecin(Date dateNaissmedecin) {
        this.dateNaissmedecin = dateNaissmedecin;
    }

    public String getSexemedecin() {
        return sexemedecin;
    }

    public void setSexemedecin(String sexemedecin) {
        this.sexemedecin = sexemedecin;
    }

    @Override
    public String toString() {
        return "Medecin{" +
                "idmedecin=" + idmedecin +
                ", nommedecin='" + nommedecin + '\'' +
                ", prenommedecin='" + prenommedecin + '\'' +
                ", adressemedecin='" + adressemedecin + '\'' +
                ", telmedecin='" + telmedecin + '\'' +
                ", dateNaissmedecin=" + dateNaissmedecin +
                ", sexemedecin='" + sexemedecin + '\'' +
                '}';
    }
}
