package com.isi.medecinservice;

import com.isi.medecinservice.dao.MedecinRepository;
import com.isi.medecinservice.entite.Medecin;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import java.util.Date;

@SpringBootApplication
public class MedecinServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedecinServiceApplication.class, args);
	}
	@Bean
	CommandLineRunner runner(MedecinRepository medecinRepository, RepositoryRestConfiguration restConfiguration){
		return args -> {
			restConfiguration.exposeIdsFor(Medecin.class);
						restConfiguration.getCorsRegistry()
					.addMapping("/**") // autoriser tout les methodes apres /
				.allowedOrigins("*") // quelque soit de domaine
					.allowedHeaders("*") // quelque soit entete
			.allowedMethods("OPTIONS","HEAD","GET","PUT","POST","DELETE","PATCH");
			for (int i=0; i<20; i++){
				medecinRepository.save(new Medecin("TAMBOURA","NOUHOUM","BAMAKO","87 654 78 54",new Date(),"M"));
				medecinRepository.save(new Medecin("DIARRA","OUMAR","BAMAKO","87 666 78 54",new Date(),"M"));
				medecinRepository.save(new Medecin("COULIBALY","OUMOU","BAMAKO","87 876 78 54",new Date(),"F"));
				medecinRepository.save(new Medecin("TRAORE","ALIOU","BAMAKO","87 111 78 54",new Date(),"M"));
				medecinRepository.save(new Medecin("CISSE","ABDOU","BAMAKO","87 444 78 54",new Date(),"M"));
			}

		};
	}
}
