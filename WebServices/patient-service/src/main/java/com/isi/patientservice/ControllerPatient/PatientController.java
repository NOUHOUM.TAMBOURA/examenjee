package com.isi.patientservice.ControllerPatient;

import com.isi.patientservice.dao.PatientRepository;
import com.isi.patientservice.entite.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
public class PatientController {
    @Autowired
    private PatientRepository patientRepository;

    @GetMapping(value = "/allPatient")
    public List<Patient> getAllPatient(){
        return patientRepository.findAll();
    }

        @PostMapping(value = "/AddPatient")
    public  Patient  AddPatient(@RequestBody Patient p ){
      return patientRepository.save(p); }
}
