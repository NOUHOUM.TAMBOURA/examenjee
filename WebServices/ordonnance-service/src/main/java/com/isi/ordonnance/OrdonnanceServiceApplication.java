package com.isi.ordonnance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//pour aciver OpenFeignClients
@EnableFeignClients
public class OrdonnanceServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdonnanceServiceApplication.class, args);
	}


}
