package com.isi.consultationservice.dao;

import com.isi.consultationservice.entite.Consultation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource

public interface ConsultationRepository extends JpaRepository <Consultation, Long> {
}
