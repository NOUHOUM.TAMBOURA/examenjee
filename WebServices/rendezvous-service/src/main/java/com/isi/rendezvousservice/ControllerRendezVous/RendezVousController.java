package com.isi.rendezvousservice.ControllerRendezVous;

import com.isi.rendezvousservice.dao.RendezVousRepository;
import com.isi.rendezvousservice.entite.RendezVous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
public class RendezVousController {

    @Autowired
    private RendezVousRepository rendezVousRepository;

    @GetMapping(value = "/AllRendezVous")
    public List<RendezVous> AllRendezVous(){
        return rendezVousRepository.findAll();
    }

}
