package com.isi.rendezvousservice.dao;

import com.isi.rendezvousservice.entite.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "PATIENT-SERVICE")
@CrossOrigin("*")
public interface PatientService {

    @GetMapping("/patients/{id}")
    public Patient findPatientById(@PathVariable(name = "id") Long id);

    // une methode qui returne la liste des patient
    @GetMapping("/patients")
    public PagedModel<Patient> findAllPatient();

    // la liste des patient de tout les patients dont ID = ---
//    @GetMapping("/patients/{id}")
//    public  PagedModel<Patient> findAllPatientByID(@PathVariable(name = "id") Long id);


}
