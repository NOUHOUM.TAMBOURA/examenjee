package com.isi.rendezvousservice.dao;

import com.isi.rendezvousservice.entite.Medecin;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.hateoas.PagedModel;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "MEDECIN-SERVICE")
@CrossOrigin("*")
public interface MedecinService {

    @GetMapping("/medecins/{id}")
    public Medecin findMedecinById(@PathVariable(name = "id") Long id);

    @GetMapping("/medecins")
    public PagedModel<Medecin> findAllMedecin();

//    @GetMapping("/medecins/{id}")
//    public  PagedModel<Medecin> findAllMedecinById(@PathVariable(name = "id") Long id);
}
