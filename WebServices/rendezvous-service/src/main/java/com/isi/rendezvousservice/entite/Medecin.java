package com.isi.rendezvousservice.entite;

import java.util.Date;

public class Medecin {
    private long idmedecin;
    private String nommedecin;
    private String prenommedecin;
    private String adressemedecin;
    private String telmedecin;
    private Date dateNaissmedecin;
    private String sexemedecin;

    public long getIdmedecin() {
        return idmedecin;
    }

    public void setIdmedecin(long idmedecin) {
        this.idmedecin = idmedecin;
    }

    public String getNommedecin() {
        return nommedecin;
    }

    public void setNommedecin(String nommedecin) {
        this.nommedecin = nommedecin;
    }

    public String getPrenommedecin() {
        return prenommedecin;
    }

    public void setPrenommedecin(String prenommedecin) {
        this.prenommedecin = prenommedecin;
    }

    public String getAdressemedecin() {
        return adressemedecin;
    }

    public void setAdressemedecin(String adressemedecin) {
        this.adressemedecin = adressemedecin;
    }

    public String getTelmedecin() {
        return telmedecin;
    }

    public void setTelmedecin(String telmedecin) {
        this.telmedecin = telmedecin;
    }

    public Date getDateNaissmedecin() {
        return dateNaissmedecin;
    }

    public void setDateNaissmedecin(Date dateNaissmedecin) {
        this.dateNaissmedecin = dateNaissmedecin;
    }

    public String getSexemedecin() {
        return sexemedecin;
    }

    public void setSexemedecin(String sexemedecin) {
        this.sexemedecin = sexemedecin;
    }

    @Override
    public String toString() {
        return "Medecin{" +
                "idmedecin=" + idmedecin +
                ", nommedecin='" + nommedecin + '\'' +
                ", prenommedecin='" + prenommedecin + '\'' +
                ", adressemedecin='" + adressemedecin + '\'' +
                ", telmedecin='" + telmedecin + '\'' +
                ", dateNaissmedecin=" + dateNaissmedecin +
                ", sexemedecin='" + sexemedecin + '\'' +
                '}';
    }
}
